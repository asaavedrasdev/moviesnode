const Router = require('koa-router')
const movieController = require('../controllers/movieController')
const joi = require('joi')
const validate = require('koa-joi-validate')


const router = new Router({
    prefix: '/movies'
})


const movieUpdatePlotValidator = validate({
    body: {
      movie: joi.string().required(),
      find: joi.string().required(),
      replace: joi.string().required()
    }
})

router.get('/', movieController.getMovies)

router.get('/:id', movieController.getMovie)

router.post('/', movieUpdatePlotValidator,movieController.updateMovie)


module.exports = router;