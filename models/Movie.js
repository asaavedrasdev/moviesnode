
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
 
const movieSchema = new Schema({
    Title: {type: String, required: true, unique: true},
    Year: {type: Number},
    Released: {type: Date},
    Genre: {type: String},
    Director: {type: String},
    Actors: { type: String},
    Plot: { type: String},
    Ratings: [{
        Source: { type: String},
        Value: { type: String}
    }]
});
 
const Movie = mongoose.model('movie', movieSchema);
module.exports = Movie;
