const movieModel = require('../models/Movie');


module.exports = {
    getMovie: async (ctx, next) => {
        try{
            const title = ctx.request.params.id
            let year = 0;
            let parameters = {};

            if(ctx.request.headers['year'] != undefined){
                year = parseInt(ctx.request.headers['year'])
                parameters['Year'] = year
            }
            parameters['Title'] = title;

            const movieByTitle = await movieModel.find(parameters)
            
            ctx.body = {
                status: 200,
                data: movieByTitle
            }
        }catch(err){
            next(err)
        }
    },
    getMovies: async (ctx, next)  => {
        try {
            const cantidad = 5;
            const rows = await movieModel.find().count()
            const pageSize = 5;
            let page = 1;
            if(rows >= cantidad && ctx.request.headers['page'] != undefined){
                page = parseInt(ctx.request.headers['page'])
            }else{
                page = 1
            }
             
            const totalPages = Math.ceil(rows / pageSize);

            if(ctx.request.headers['page'] || rows > cantidad){
                const movies = await movieModel
                            .find()
                            .skip((page-1) * pageSize)
                            .limit(pageSize)
                ctx.body = {
                    status: 200,
                    pageSize: pageSize,
                    page: page,
                    totalRows: rows,
                    totalPages: totalPages,
                    data: movies
                }

            }else {
                ctx.body = await movie.find();
            }
            
        }catch(err){
            next(err)
        }
    },
    updateMovie: async(ctx,next) => {
        try{
            const { movie, find, replace} = ctx.request.body

            const movieByTitle = await movieModel.findOne({movie: movie})
            const plot = movieByTitle.Plot
            const findExpresion = new RegExp(find, 'g');
            const plotUpdate = plot.replace(findExpresion,replace)

            const movieUpdate = await movieModel.findOneAndUpdate({movie:movie},{Plot:plotUpdate})

            ctx.body = {
                status: 200,
                Plot:plotUpdate
            }
            
        }catch(err){
            next(err)
        }
    }
}