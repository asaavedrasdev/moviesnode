const mongoose = require('mongoose')

const connectDatabase = async () => {
    const conexion = await mongoose.connect(process.env.MONGO_URI,{
        useNewUrlParser: true
    })
    console.log('Mongodb conectado')
}

module.exports = connectDatabase;