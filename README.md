# moviesnode

1. Para ejecutar la aplicación primero de debe ejecutar **docker-compose up -d --build** en la carpeta raiz del repositorio.

2. Para importar los datos a la bdd se deben ejecutar los siguientes comandos: **docker cp movies.json moviesnode_mongo_1:/tmp/movies.json** y **docker exec moviesnode_mongo_1 mongoimport -d movienode -c movies --file /tmp/movies.json**.

2. Para acceder a la ruta de swagger se de ir a la siguiente url:**http://localhost:3000/swagger**.


