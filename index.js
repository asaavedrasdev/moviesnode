const koa = require('koa')
const dotenv = require('dotenv')
const connectDatabase = require('./config/db')
const bodyParser = require('koa-bodyparser')
const movieRoutes = require('./router/movie')
const errorHandler= require('./middlewares/error');
const swagger = require("swagger2");
const { ui } = require("swagger2-koa");
const logger = require('koa-logger')

dotenv.config({path:'./config/config.env'})


connectDatabase()

const app = new koa()

const swaggerDocument = swagger.loadDocumentSync("./config/api.yaml")

app.use(ui(swaggerDocument, "/swagger"))
app.use(bodyParser())
app.use(errorHandler)
app.use(logger())

app.use(movieRoutes.routes())
   .use(movieRoutes.allowedMethods())


app.listen(3000,()=> console.log('Servicio ejecutandose en puerto : 3000'))